import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './models/Pic.dart';

import './login_page.dart';
import './screens/home.dart';
import 'screens/details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget _getLandingPage() {
    return StreamBuilder<FirebaseUser>(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.providerData.length == 1) {
            return snapshot.data.isEmailVerified
                ? Home(pics: getPics())
                : LoginPage();
          } else {
            return Home(pics: getPics());
          }
        } else {
          return LoginPage();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.white,
      ),
    );
    return MaterialApp(
      initialRoute: '/', // default is '/' home (here screen) directory jesa
      routes: {
        '/': (ctx) => _getLandingPage(),
        Home.routeName: (ctx) => Home(),
        PicDetailScreen.routeName: (ctx) => PicDetailScreen(),
      },
      debugShowCheckedModeBanner: false,
      title: 'Pinterest',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
    );
  }
}
