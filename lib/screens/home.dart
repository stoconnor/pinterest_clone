import 'package:flutter/material.dart';

import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import './../models/Pic.dart';
import '../widgets/PicCard.dart';


class Home extends StatelessWidget {
   static const routeName = '/home';
  final Future<List<Pic>> pics;
   Home({this.pics});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Icon(
                Icons.search,
                color: Colors.grey,
                size: 40.0,
              ),
              Container(
                width: 160.0,
                child: Image.asset('assets/images/pinterest-logo.png'),
                decoration: BoxDecoration(
                    border: Border(
                        bottom:
                            BorderSide(width: 3.0, color: Colors.red[800]))),
              ),
              Icon(
                Icons.account_circle,
                color: Colors.grey,
                size: 40.0,
              )
            ],
          ),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          color: Colors.grey[400],
          child: FutureBuilder(
            future: pics,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return StaggeredGridView.countBuilder(
                  crossAxisCount: 4,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return PicCard(item: snapshot.data[index]);
                  },
                  staggeredTileBuilder: (index) {
                    return StaggeredTile.count(2, index.isEven ? 2 : 5);
                  },
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  
                );
                
              } else if (snapshot.hasError) {
                return Text(snapshot.error);
              }
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.red[800]),
                ),
              );
            },
          ),
        ),
      );
  }}