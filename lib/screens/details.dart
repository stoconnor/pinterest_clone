import 'package:flutter/material.dart';
import '../models/Pic.dart';


class PicDetailScreen extends StatelessWidget {
  static const routeName = '/pic-detail';
  final Pic item;
  PicDetailScreen({this.item});

  @override
   
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(22, 24, 31, 1.0),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(22, 24, 31, 1.0),
        centerTitle: true,
        title: Text(item.author),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Image.network(
              item.url,
              fit: BoxFit.fill,
            ),
          ],
        ),
      ),
    );
  }
}