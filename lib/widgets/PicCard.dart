import 'package:flutter/material.dart';
import 'package:pinterest_clone/screens/details.dart';
import 'dart:math';

import './../models/Pic.dart';

class PicCard extends StatelessWidget {
  final Pic item;
  final _random = Random();

  PicCard({this.item});

  int randomInt(int min, int max) => min * _random.nextInt(max - min);
    void selectCategory(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
     PicDetailScreen.routeName,
      arguments: {
        'pic':this.item
      }
    );
  }
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 3.0,
      child: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: MaterialButton(
                onPressed: () => selectCategory(context),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2.5)),
                      image: DecorationImage(
                          image: NetworkImage(item.url), fit: BoxFit.cover)),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 12.0,
              ),
              child: Column(
                children: <Widget>[
                  Text(item.author,
                      style: TextStyle(
                        color: Colors.grey[800],
                        fontSize: 16.0,
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
