import 'package:http/http.dart' as http;
import 'dart:convert';

class Pic {
  String author;
  String url;

  Pic({this.author, this.url});

  toJson() {
    return {"author": this.author, "url": this.url};
  }

  factory Pic.fromJson(json) {
    return Pic(author: json['author'], url: json['download_url']);
  }
}

Future<List<Pic>> getPics() async {
  final response = await http.get('https://picsum.photos/v2/list');
  if (response.statusCode == 200) {
    final resData = json.decode(response.body);
    List<Pic> pics = [];
    resData.forEach((p) {
      final Pic pic = Pic.fromJson(p);
      pics.add(pic);
    });
    return pics;
  } else {
    throw Exception("Failed to load pics");
  }
}